'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config/config'),
    ObjectId = Schema.ObjectId,
    async = require('async'),
    crypto = require('crypto');


// User Schema

var UserSchema = new Schema({
  user_name  : String,
  company_name : String,
  name  : String ,
  hashed_password   : {
    type: String
  },
  salt :  String,
  email : {
        type: String,
        required: true,
        unique: true
  },
  provider : String,
  userType: {
    type: Number // 1: guest user, 2: registered user, 4: admin
  },
  subscription: [String],
  companyName: String,
  subscriptionEndDate: Date,
});

/**
 * Virtuals
 */
UserSchema.virtual('password').set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
}).get(function() {
    return this._password;
});

/**
 * Validations
 */
var validatePresenceOf = function(value) {
    return value && value.length;
};

// The below 5 validations only apply if you are signing up traditionally
UserSchema.path('email').validate(function(email) {
    if (this.provider !== 'local') return true;
    else return email.length;
}, 'Email cannot be blank');
UserSchema.path('hashed_password').validate(function(hashed_password) {
    if (this.provider !== 'local') return true;
    else return hashed_password.length;
}, 'Password cannot be blank');

/**
 * Pre-save hook
 */
UserSchema.pre('save', function(next) {

    if (!this.isNew) return next();

    if (!validatePresenceOf(this.password) && this.provider === 'local') next(new Error('Invalid password'));
    else next();
});

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashed_password;
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */
    makeSalt: function() {
        return Math.round((new Date().valueOf() * Math.random())) + '';
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */
    encryptPassword: function(password) {
        if (!password) return '';
        return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    }
};



UserSchema.plugin(require('mongoose-timestamp'));
var Users = mongoose.model('Users', UserSchema);



//var allUsers = [{
//        'email': 'admin@gmail.com',
//        'password': 'administrator@321',
//        'name': 'Administrator',
//        'provider': 'local',
//    }, {
//        'email': 'praveen.kumar3489@gmail.com',
//        'password': 'pass@1234',
//        'name': 'Administrator',
//        'provider': 'local',
//    }
//];
//
//async
//    .map(allUsers,
//        function(item, cb) {
//            Users.count({
//                email: item.email
//            }, function(err, count) {
//                if (!count) {
//                    new Users(item).save(cb);
//                    // Users.ensureIndex({point:"2dsphere"});
//                } else {
//                    // Handle err.
//                    // console.log('Already Exists');
//                }
//            })
//        },
//        function(err, result) {
//            console.log(err);
//            // console.log(result);
//        });
