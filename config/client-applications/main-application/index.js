// Note: RESOURCE ACCESS URL IS RELATIVE TO PUBLIC DIRECTORY

module.exports = {
  accessUrl : '/',
  indexFileUrl : '/views/main.jade',
  metaData : {
  	title : 'CART',
  	description : 'My-project-description',
  	keywords : 'tag1, tag2, tag3',
  	siteName : 'site_name',
  	appID : 'AppID',
  	url : 'Url',
  	// favicon : '/img/favicon.ico',
  	image : '/img/image.png',
  	type : 'website'
  },
  angular : {
    globalModlueName : 'My-AdminProject',
    globalDependencies : ['ui.router', 'ui.bootstrap','ui.router.stateHelper','ngBootstrap','ngCookies', 'angular-growl'],
    modules : ['home'],// ['core', 'LR', 'home'],
  },
  css : {
    vendor : [
      '/lib/bower/bootstrap/dist/css/bootstrap.min.css',
      '/lib/bower/bootstrap/dist/css/bootstrap-theme.min.css',
      '/lib/bower/font-awesome/css/font-awesome.min.css',
      '/lib/bower/bootstrap-daterangepicker/daterangepicker-bs3.css',
      '/lib/bower/angular-growl/build/angular-growl.min.css',
    ],
    custom : [
    '/style-sheets/common.css',
    ]
  },

  js : {
    vendor : [
      '/lib/bower/lodash/lodash.min.js',
      '/lib/bower/jquery/dist/jquery.min.js',
      // '/lib/bower/angular/angular.min.js',
      '/lib/bower/angular/angular.js',
      '/lib/bower/angular-bootstrap/ui-bootstrap-tpls.min.js',
      '/lib/bower/moment/min/moment.min.js',
      '/lib/bower/bootstrap-daterangepicker/daterangepicker.js',
      '/lib/bower/angular-cookies/angular-cookies.min.js',
      // '/lib/bower/angular-daterangepicker/js/angular-daterangepicker.min.js',
      '/lib/bower/angular-ui-router/release/angular-ui-router.min.js',
      '/lib/bower/angular-ui-router.stateHelper/statehelper.min.js',
      '/lib/bower/angular-growl/build/angular-growl.min.js',
      '/lib/bower/ng-bs-daterangepicker/src/ng-bs-daterangepicker.js'

    ],
    custom : [
    ],
    test : [
      '/lib/bower/angular-mocks/angular-mocks.js',
      '/modules/*/tests/*.js'
    ]
  }
};
