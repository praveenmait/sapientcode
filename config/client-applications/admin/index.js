// Note: RESOURCE ACCESS URL IS RELATIVE TO PUBLIC DIRECTORY

module.exports = {
  accessUrl : '/admin',
  indexFileUrl : '/views/admin.jade',
  metaData : {
  	title : 'CART',
  	description : 'My-project-description',
  	keywords : 'tag1, tag2, tag3',
  	siteName : 'site_name',
  	appID : 'AppID',
  	url : 'Url',
  	// favicon : '/img/favicon.ico',
  	image : '/img/image.png',
  	type : 'website'
  },
  angular : {
    globalModlueName : 'My-AdminProject',
    globalDependencies : ['ngResource', 'ui.router', 'ui.bootstrap','ui.router.stateHelper', 'angular-growl', 'ngTagsInput', 'angularFileUpload', 'angular-loading-bar'],
    modules : ['admin-app'],// ['core', 'LR', 'home'],
  },
  css : {
    vendor : [
      '/lib/bower/bootstrap/dist/css/bootstrap.min.css',
      '/lib/bower/bootstrap/dist/css/bootstrap-theme.min.css',
      '/lib/bower/font-awesome/css/font-awesome.min.css',
      '/lib/bower/angular-growl/build/angular-growl.min.css',
      '/lib/bower/ng-tags-input/ng-tags-input.min.css',
      '/lib/bower/angular-loading-bar/src/loading-bar.css',
      '/lib/bower/select2/select2.css',
      '/lib/bower/angular-ui-select/dist/select.min.css',
    ],
    custom : [
      '/style-sheets/common.css',
      '/style-sheets/style.css',
      '/style-sheets/jquery.gritter.css',
      '/style-sheets/nanoscroller.css',
      '/style-sheets/bootstrap-switch.css',
      // '/style-sheets/select2.css',
      '/style-sheets/slider.css',
      '/style-sheets/css.css',
      '/style-sheets/css(1).css',
      '/style-sheets/css(2).css',
    ]
  },

  js : {
    vendor : [
      '/lib/bower/lodash/lodash.min.js',
      '/lib/bower/jquery/dist/jquery.min.js',
      // '/lib/bower/angular/angular.min.js',
      '/lib/bower/angular/angular.js',
      '/lib/bower/angular-bootstrap/ui-bootstrap-tpls.min.js',
      '/lib/bower/angular-ui-router/release/angular-ui-router.min.js',
      '/lib/bower/angular-ui-router.stateHelper/statehelper.min.js',
      '/lib/bower/angular-growl/build/angular-growl.min.js',
      '/lib/bower/bootstrap/dist/js/bootstrap.min.js',
      '/lib/bower/angular-resource/angular-resource.min.js',
      '/lib/bower/angular-bootstrap/ui-bootstrap-tpls.min.js',
      // '/lib/bower/angular-ui-utils/ui-utils.min.js',
      '/lib/bower/angular-ui-router/release/angular-ui-router.min.js',
      '/lib/bower/angular-ui-router.stateHelper/statehelper.min.js',
      '/lib/bower/ng-tags-input/ng-tags-input.min.js',
      '/lib/bower/angular-file-upload/angular-file-upload.min.js',
      '/lib/bower/angular-loading-bar/src/loading-bar.js',

      // '/js/bootstrap.min.js',
      // '/js/jquery.js',
      '/js/general.js',
      '/js/jquery.gritter.js',
      '/js/jquery.nanoscroller.js',
      '/js/jquery-ui.js',
      '/js/jquery.sparkline.min.js',
      '/js/jquery.easy-pie-chart.js',
      '/js/jquery.nestable.js',
      '/js/bootstrap-datetimepicker.min.js',
      // '/js/select2.min.js',
      '/js/skycons.js',
      '/js/bootstrap-slider.js',
      '/js/intro.js',
      '/lib/bower/select2/select2.min.js',
      '/lib/bower/angular-ui-select/dist/select.min.js',
    ],
    custom : [
    ],
    test : [
      '/lib/bower/angular-mocks/angular-mocks.js',
      '/modules/*/tests/*.js'
    ]
  }
};
