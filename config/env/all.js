/**
 * Common config for all environments
 * Overwrite for specific env in that env's file
 */

'use strict';

var path = require('path'),
    rootPath = path.normalize(__dirname + '/../..');
module.exports = {
    app: {
        title: 'Meeting',
        description: 'My-project-description',
        keywords: 'My-project-keywords'
    },
    db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI,
    root: rootPath,
    port: process.env.PORT || 3000,
    templateEngine: 'jade',
    sessionSecret: 'My-secret-key',
    sessionCollection: 'new-sessions',
    mailers: {
        // support: "Uberstarter Support ✔ <support@uberstarter.com>",
        // community: "Uberstarter Community ✔ <community@uberstarter.com>",
        // blog: "Uberstarter Blog ✔ <blog@uberstarter.com>",
        // messaging: "Uberstarter Messaging ✔ <messaging@uberstarter.com>",
        // activity: "Uberstarter Acitivity ✔ <activity@uberstarter.com>"
    }
};
