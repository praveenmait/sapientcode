'use strict';

module.exports = {
    db: '',
    s3: {
        // "accessKeyId": "",
        // "secretAccessKey": "",
        // "region": "",
        // "bucketName": '',
        // "awsAccountId": ''
        'acl': 'public-read'
    },
    ses: {
        // "accessKeyId": "",
        // "secretAccessKey": "",
        // "serviceUrl": 'https://email.us-west-2.amazonaws.com'
    },
    esPrefix: 'dev' //Elastic search index prefix
};
