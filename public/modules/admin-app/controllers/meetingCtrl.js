angular.module('admin-app')

.controller('MeetingCtrl', ['$http', '$scope', '$modal', '$state', 'growl',
    function($http, $scope, $modal, $state, growl) {
      $scope.MeetingData = {};

      $scope.listAll =  function(){
        $http.get('/meeting/all')
          .success(function(data) {
              if (data.error) {
                  var message = data.error.message
                  alert(message)
              } else {
                $scope.MeetingData= data.response
              }
          })
          .error(function(data) {
              console.log("error", data)
          });
      }

      $scope.listAll();
    }])

.controller('ListUserCtrl', ['$http', '$scope', '$state', 'growl', function($http, $scope, $state, growl){
  $http.get('/users')
    .success(function(data){
        $scope.users = data
    })
}])
