angular.module('admin-app')

.config(function(stateHelperProvider, $locationProvider, $urlRouterProvider){
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/login');
  // $urlRouterProvider.otherwise('/');
  stateHelperProvider

  .setNestedState({
            name: "login",
            url: '/login',
            templateUrl: "templates/modules/admin-app/views/login",
            controller: "AdminLoginCtrl",
            module: 'public',
        })
  .setNestedState({
            name: "register",
            url: '/register',
            templateUrl: "templates/modules/admin-app/views/register",
            controller: "AdminLoginCtrl",
            module: 'public',
        })
    .setNestedState({
            name: "adminhome",
            url: '/index',
            templateUrl: "templates/modules/admin-app/views/admin",
            controller: "AdminLoginCtrl",
            // abstract: true,
            // module: 'public',
            children: [{
                name: "list",
                url: '/list?page&perPage',
                views: {
                    "AdminView": {
                        templateUrl: "templates/modules/admin-app/views/list",
                        controller: "MeetingCtrl",
                    }
                }
              },{
                  name: "userlist",
                  url: '/list',
                  views: {
                      "AdminView": {
                          templateUrl: "templates/modules/admin-app/views/listUser",
                          controller: "ListUserCtrl",
                      }
                  }
                }]
        })
})
