angular.module('home-app')

.config(['stateHelperProvider', '$locationProvider', '$urlRouterProvider', '$cookiesProvider', '$sceProvider',function(stateHelperProvider, $locationProvider, $urlRouterProvider, $cookiesProvider, $sceProvider){
  // console.log($sceProvider)
  // $sceProvider.enabled(false);
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');
  stateHelperProvider

  // .setNestedState({
  //           name: "login",
  //           url: '/',
  //           templateUrl: "templates/modules/home/views/cart",
  //           controller: "loginCtrl",
  //       })
  .setNestedState({
            name: "main",
            url: '',
            templateUrl: "templates/modules/home/views/layout",
            module: 'public',
            children: [{
                  name: "list",
                  url: '/',
                  views: {
                      "cartView": {
                          templateUrl: "templates/modules/home/views/listing",
                          controller: "productCtrl",
                      }
                  }
            },{
                  name: "cart",
                  url: '/cart',
                  views: {
                      "cartView": {
                          templateUrl: "templates/modules/home/views/cart",
                          controller: "cartCtrl",
                      }
                  },
                  children: [{
                    name: "payment",
                    url: '/payment',
                    views: {
                        "paymentView": {
                            templateUrl: "templates/modules/home/views/paymentView",
                            controller: "cartCtrl",
                        }
                    }
                  }]
            }]
        })
  .setNestedState({
            name: "register",
            url: '/register',
            templateUrl: "templates/modules/home/views/register",
            controller: "loginCtrl",
            module: 'public',
        })
  // $locationProvider.html5Mode(true);
}])



// .run(['$state', '$cookies', '$rootScope' ,'Self', '$location', function($state, $cookies, $rootScope, Self,$location) {
//     // $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
//     //     // console.log("xyz")
//     //     if (toState.module === 'public' && window.__user) {
//     //         e.preventDefault();
//     //         // If logged in and transitioning to a logged out page:
//     //         $state.go('home.home.news');
//     //     } else if (toState.module === 'private' && !window.__user) {
//     //         // If logged out and transitioning to a logged in page:
//     //         e.preventDefault();
//     //         $state.go('login');
//     //     };
//     // });
//     // $rootScope.$on("$locationChangeStart", function(event, next, current) {
//     //     console.log(current, event, next);
//     //     console.log('newsData', $rootScope.newsData);

//     //     // #!/fetch restricter
//     //     if(/#!\/fetch/.test(next)){
//     //         var id = next.split("cid=")[1];

//     //         _.forIn($rootScope.newsData, function (value, key) {
//     //             isAllowed =
//     //         })
//     //     }
//     // });
// }])
