angular.module('home-app')

.controller('productCtrl',['$scope', '$http', '$state', '$rootScope', function($scope, $http, $state, $rootScope){
    $scope.products={};
    $rootScope.cart = [];
    $rootScope.itemInCart = []
    $scope.productList = function(){
        $http.get('http://jsonp.afeld.me/?url=https://api.myjson.com/bins/19ynm')
            .success(function(data) {
              $scope.products = data.productsInCart;
            })
            .error(function(data){
              alert("error: "+data)
            })
        }
    $scope.productList();

    $scope.addToCart = function(item){
        $rootScope.cart.push(item);
        $rootScope.itemInCart.push(item.p_id);
    }

    $scope.removeFromCart = function(p_id){
      var index = $rootScope.itemInCart.indexOf(p_id);
      if (index > -1) {
          $rootScope.itemInCart.splice(index, 1);
          $rootScope.cart.splice(index, 1);
          _($scope.products).forEach(function(product, index) {
              $scope.products[index]['isDisabled'] =false;
            });
      }
    }
}])


.controller('cartCtrl',['$scope', '$http', '$state', '$modal', '$rootScope', function($scope, $http, $state, $modal, $rootScope){
  $scope.cartItems = $rootScope.cart || [];
  $scope.itemInCart = $rootScope.itemInCart || [];
  $scope.promocode = 'SP0';
  $scope.discountAmt = 0;
  $scope.shippingAmt = 0;

  $scope.removeItem = function(index) {
      $scope.cartItems.splice(index, 1);
  }

  $scope.total = function() {
    var total = 0;
    angular.forEach($scope.cartItems, function(item){
      total += item.p_quantity * item.p_price;
    })
    return total;
  }

  $scope.shipping = function(){
    var shipping = 0;
    if ($scope.total() > 50){
        shipping =5;
    }
    $scope.shippingAmt = shipping;
    return shipping;
  }

  $scope.discount = function(){
    var discount = 0;
    if (($scope.promocode == 'SP5') || ($scope.itemInCart.length >= 3)){
      $scope.promocode = 'SP5'
      discount = .05 * $scope.total();
    } else if (($scope.promocode == 'SP10') || (($scope.itemInCart.length > 3) && ($scope.itemInCart.length <= 6))) {
      $scope.promocode = 'SP10'
      discount = .10 * $scope.total();
    } else if (($scope.promocode == 'SP25') || ($scope.itemInCart.length > 6)) {
      $scope.promocode = 'SP25'
      discount = .25 * $scope.total()
    }
    $scope.discountAmt = parseFloat(discount).toFixed(2);
    return discount
  };


  $scope.estimatedAmount = function(){
    var estimatedAmount = 0;
    estimatedAmount = $scope.total() + $scope.shipping() - $scope.discount();
    return estimatedAmount;
  }

  $scope.edit = function(index){
    var modalInstance = $modal.open({
        templateUrl: 'templates/modules/home/views/editView',
        controller: 'modalCtrl',
        resolve: {
          item: function () {
            return $scope.cartItems[index];
          }
       }
      })
  }

}])
