angular.module('home-app')

.controller('modalCtrl',['$scope', '$http', '$modalInstance' ,'item', function($scope, $http, $modalInstance, item){
    $scope.item = item
    $scope.closeModal = function(){
       $modalInstance.close();
    }
}])
