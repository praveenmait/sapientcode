angular.module('home-app')

.controller('homeCtrl',['$scope', '$http', '$state', '$rootScope', function($scope, $http, $state, $rootScope){
    $scope.logout = function(){
        $http.post('/users/logout')
            .success(function(data) {
              delete window.__user
              $state.go("login");
        })
    }
}])
