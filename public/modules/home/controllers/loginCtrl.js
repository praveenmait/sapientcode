angular.module('home-app')

.controller('loginCtrl',['$scope', '$http', '$state', '$rootScope', 'growl', function($scope, $http, $state, $rootScope, growl){
    $scope.user={}

    $scope.login = function(){
        $http.post('/user/login', $scope.user)
            .success(function(data) {
                if (data.error) {
                    var message = data.error.message
                    alert(message)
                } else {
                  $scope.userData= data
                  $state.go("home.create");
                }

            })
            .error(function(data) {
                growl.addErrorMessage("Invalid Credentials");
            });
    }
    $scope.register = function(){
      $scope.user.type=2;
      $http.post('/user/register', $scope.user)
          .success(function(err, data) {
              if (data==200) {
                  console.log("err", err)
                  growl.addSuccessMessage(JSON.stringify(err) + "data" + data);
                  $state.go("home.create");
              } else{
                growl.addSuccessMessage("User successfully registered");
              }

          })
          .error(function(err, data) {
              growl.addErrorMessage("Bad Request: Username name already exists.");
          });
    }
}])
